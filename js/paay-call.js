var paayformArr = ['checkout_form', 'is-upsell', 'downsell_form1'];
var errorClass = 'has-error';
var validClass = 'no-error';
var dev_mode = app_config.dev_mode;
var timeout = 5;
var enable_loader = true;
var count = 0;
var totalInitialisation = 0;
var isTrigger = false;
var Cavv, Eci, Xid;
var prevString = '';
var is_process_started = false;
var isLegacy = true;

// New Variable for New version

var initial_xid;
var initial_cavv;
var initial_eci;
var initial_authenticationValue;
var initial_dsTransactionId;
var initial_status;
var initial_protocolVersion;

var rebill_xid;
var rebill_cavv;
var rebill_eci;
var rebill_authenticationValue;
var rebill_dsTransactionId;
var rebill_status;
var rebill_protocolVersion;

var split_xid;
var split_cavv;
var split_eci;
var split_authenticationValue;
var split_dsTransactionId;
var split_status;
var split_protocolVersion;

var split_rebill_xid;
var split_rebill_cavv;
var split_rebill_eci;
var split_rebill_authenticationValue;
var split_rebill_dsTransactionId;
var split_rebill_status;
var split_rebill_protocolVersion;
 
$(function () {
    var formName = $('form').attr('name');

    var mainhtml = '';
    var apiData = {
        amount: 'amount',
        expmonth: 'month',
        expyear: 'year',
        creditCardNumber: 'pan'
    };   

    if ((paayformArr.indexOf(formName) != -1) && typeof paay_3D_settings !== 'undefined')
    {
        timeout = paay_3D_settings['timeout'];
        enable_loader = paay_3D_settings['enable_loader'];
        event_type = paay_3D_settings['verify_on_submit'];
        if(paay_3D_settings.version == 2)
        {
            isLegacy = false;
        }
        
        if(timeout == '')
        {
            timeout = 5;
        }
        else{
            timeout = parseInt(timeout);
        }
        
        var currStp = paay_3D_settings.step_id;
        var pstep = paay_3D_settings['step' + currStp];   
//        if (!pstep.main)
//        {
//            return;
//        }

        if ($('form[name=' + formName + ']').length)
        {
            $('head').append(
                    '<link rel="stylesheet" type="text/css" href="' +
                    paay_3D_settings.paay_extension_url +
                    'css/paay.css">'
                    );
            
            
            for (var key in apiData) {
                if ($('[name=' + key + ']').length)
                {
                    $('[name=' + key + ']').attr('data-threeds', apiData[key]);
                }
            }

            $('form[name=' + formName + ']').append('<input type="hidden" name="x_transaction_id" value="'+randomString()+'" data-threeds="id" />')
            $('form[name=' + formName + ']').append('<input type="hidden" id="is_verify_done" value="N" />'); 

            if (pstep.main)
            {
                mainhtml += '<input type="hidden" name="cavv" id="cavv" />';
                mainhtml += '<input type="hidden" name="eci" id="eci" />';
                mainhtml += '<input type="hidden" name="xid" id="xid" />';
                mainhtml += '<input type="hidden" name="status" id="status" />';
                mainhtml += '<input type="hidden" id="amt" name="total_price" data-threeds="amount" value="' + pstep.main_price + '">';
                if(!isLegacy)
                {
                    mainhtml += '<input type="hidden" name="protocolVersion" id="protocolVersion" />';
                    mainhtml += '<input type="hidden" name="authenticationValue" id="authenticationValue" />';
                    mainhtml += '<input type="hidden" name="dsTransactionId" id="dsTransactionId" />';
                }
                
            }
            
            if (pstep.rebill || paay_3D_settings.skip_trial_protection)
            {   
                mainhtml += '<input type="hidden" name="rebill_cavv" id="rebill_cavv" />';
                mainhtml += '<input type="hidden" name="rebill_eci" id="rebill_eci" />';
                mainhtml += '<input type="hidden" name="rebill_xid" id="rebill_xid" />';
                mainhtml += '<input type="hidden" name="rebill_status" id="rebill_status" />';
                mainhtml += '<input type="hidden" name="rebill_protocolVersion" id="rebill_protocolVersion" />';
                mainhtml += '<input type="hidden" name="rebill_authenticationValue" id="rebill_authenticationValue" />';
                mainhtml += '<input type="hidden" name="rebill_dsTransactionId" id="rebill_x`dsTransactionId" />';
                
                if(!(paay_3D_settings.skip_trial_protection))
                {
                    mainhtml += '<input type="hidden" id="rebill_amt" name="total_price" data-threeds="amount" value="' + pstep.rebill_price + '">';
                }
            }

            if (pstep.split_main)
            {
                mainhtml += '<input type="hidden" name="split_cavv" id="split_cavv" />';
                mainhtml += '<input type="hidden" name="split_eci" id="split_eci" />';
                mainhtml += '<input type="hidden" name="split_xid" id="split_xid" />';
                mainhtml += '<input type="hidden" name="split_status" id="split_status" />';
                mainhtml += '<input type="hidden" id="split_amt" name="split_total_price" data-threeds="amount" value="' + pstep.split_main_price + '">';
                if(!isLegacy)
                {
                    mainhtml += '<input type="hidden" name="split_protocolVersion" id="split_protocolVersion" />';
                    mainhtml += '<input type="hidden" name="split_authenticationValue" id="split_authenticationValue" />';
                    mainhtml += '<input type="hidden" name="split_dsTransactionId" id="split_dsTransactionId" />';
                }
                
            }

            if (pstep.split_rebill)
            {
                mainhtml += '<input type="hidden" name="split_rebill_cavv" id="split_rebill_cavv" />';
                mainhtml += '<input type="hidden" name="split_rebill_eci" id="split_rebill_eci" />';
                mainhtml += '<input type="hidden" name="split_rebill_xid" id="split_rebill_xid" />';
                mainhtml += '<input type="hidden" name="split_rebill_status" id="split_rebill_status" />';
                mainhtml += '<input type="hidden" id="split_rebill_amt" name="split_rebill_total_price" data-threeds="amount" value="' + pstep.split_rebill_price + '">';
                if(!isLegacy)
                {
                    mainhtml += '<input type="hidden" name="split_rebill_protocolVersion" id="split_rebill_protocolVersion" />';
                    mainhtml += '<input type="hidden" name="split_rebill_authenticationValue" id="split_rebill_authenticationValue" />';
                    mainhtml += '<input type="hidden" name="split_rebill_dsTransactionId" id="split_rebill_dsTransactionId" />';
                }
                
            }
            
            if (currStp > 1)
            {
                mainhtml += '<input name="creditCardType" value="' + pstep.cardType + '"  type="hidden"/>';
                mainhtml += '<input name="expmonth" value="' + pstep.expmonth + '" type="hidden" data-threeds="month" />';
                mainhtml += '<input name="expyear" value="' + pstep.expyear + '"  type="hidden" data-threeds="year"/>';
                mainhtml += '<input name="creditCardNumber" value="' + pstep.creditCardNumber + '"  type="hidden" data-threeds="pan"/>';
            }
            $('form[name=' + formName + ']').append(mainhtml);           
            
        }
        
        if(event_type == 'blur') {
            if(currStp == 1)
            {
                $('input[name=creditCardNumber]').blur(function(){
                    checkCardData();
                });

                $('select[name=expmonth],select[name=expyear]').change(function(){
                    checkCardData();
                });

            }

            if(currStp > 1)
            {
                setTimeout(function(){
                    if(placeCookies())
                    {
                        return;
                    }
                    checkCardData();
                },1000);  
            }
        }

        
    }

    function randomString() {
        return 'id-' + Math.random().toString(36).substr(2, 16);
    }
    
    function checkCardData()
    {
        return new Promise(function (done, notdone)
        {
            var errors = [];
            var formName = $('form').attr('name');
            if (paayformArr.indexOf(formName) == -1 || typeof paay_3D_settings === 'undefined')
            {
                return done();
            }          
            
            if (!$('form[name=' + formName + ']').length)
            {
                return done();
            } 
            
            var errors = [];
            var selector = '';
            if (formName == 'is-upsell')
            {
                selector = 'input';
            }
            else
            {
                selector = 'select';
            }

            var validCCType = ['visa', 'master','amex', 'discover'];
            var m = $(selector + '[name=expmonth]').val();
            var y = $(selector + '[name=expyear]').val();
            var cc = $('input[name=creditCardNumber]').val();
            var ccType = $(selector + '[name=creditCardType]').val();
            var currStp = paay_3D_settings.step_id;

            if($.inArray(ccType, validCCType) == '-1' && currStp == 1) {
                console.log('Excluding paay call');
                return done();
            }
            
            var binVerify = paay_3D_settings.enable_bin_verify;
            if(typeof(binVerify) != 'undefined' && binVerify) {
                var unmaskedCardNumber = cc.trim().replace(/[\s-]/g, '');
                if(typeof(unmaskedCardNumber) != 'undefined' && unmaskedCardNumber.length < 6) {
                    return done();
                }
                var binData = cb.decryptAllowedTestCard(paay_3D_settings.bin);
                var binArray = binData.split('\n');
                var checkingType = paay_3D_settings.action_on_bin;
                var bin = unmaskedCardNumber.substr(0,6);
                var isBinInList = $.inArray(bin, binArray);
                if(typeof(checkingType) != 'undefined' && checkingType == 'exclude' && isBinInList != '-1') {
                    if(typeof(dev_mode) != 'undefined' && dev_mode == 'Y')
                    {
                        console.log('Excluded');
                    }
                    return done();
                }
                
                if(typeof(checkingType) != 'undefined' && checkingType == 'include' && isBinInList == '-1') {
                    if(typeof(dev_mode) != 'undefined' && dev_mode == 'Y')
                    {
                        console.log('Not Included');
                    }
                    return done();
                }               
            }

            cc = cc.replace(/-/g, '');
            var t = $('[name=x_transaction_id]').val();//randomString(6);
            var amt = parseFloat($('#amt').val());
            var rebillAmt = parseFloat($('#rebill_amt').val());
            var pstep = paay_3D_settings['step' + currStp];  
            
            /*
             * Start New Card Type Filter Logic
             */
            var cardTypeSettings = pstep['card_type']; 
            if(typeof(cardTypeSettings) != 'undefined' && cardTypeSettings.length) {
                paay_3D_settings.only_rebill = false;
                pstep.rebill = true;
                pstep.main = true;
                
                if(
                    cardTypeSettings.indexOf('rebill_'+ccType) == -1 &&  
                    cardTypeSettings.indexOf('rebill_initial_'+ccType) == -1 &&
                    cardTypeSettings.indexOf('initial_rebill_'+ccType) == -1)
                {
                    pstep.rebill = false;
                }
                
                if(
                    cardTypeSettings.indexOf('initial_'+ccType) == -1 &&  
                    cardTypeSettings.indexOf('rebill_initial_'+ccType) == -1 &&
                    cardTypeSettings.indexOf('initial_rebill_'+ccType) == -1)
                {                    
                    pstep.main = false;
                    paay_3D_settings.only_rebill = true;
                }
            } 
            /*
             * End New Card Type Filter Logic
             */
            
            if (!pstep.main && paay_3D_settings.only_rebill)
            {
                amt = rebillAmt;
                rebillAmt = '';
            }
            
            var errorflag = false;
            var tds;

            if (m != '' && y != '' && cc != '' && t != '' && amt != '' && (pstep.main || pstep.rebill))
            {
                if(is_process_started)
                {
                    console.log('ANOTHER PROCESS IS ALREADY RUNNING, PLEASE WAIT...');
                    return done();
                }
                if(prevString!= '' && prevString == (m+y+cc))
                {
                    console.log('NOTHING HAS CHANGED');
                    return done();
                }
                
                isTrigger = true;
                $('#is_verify_done').val('N');
                console.log('after initial:', amt, cc, m, y, t);
                var strict_method = paay_3D_settings['strict_method'];
                if(strict_method)
                {
                    $('form').find('[type=submit]').attr('disabled','disabled');
                }
                var formid = $('form[name=' + formName + ']').attr('id');
                var postData = {autoSubmit:false, allowRetry: false, iframeId: "paay-iframe"};
                if(typeof(dev_mode) != 'undefined' && dev_mode == 'Y')
                {
                    postData['verbose'] = true;
                }
                
                if (typeof formid === 'undefined')
                {
                    $('form[name=' + formName + ']').attr('id', formName);
                    formid = formName;
                }
                var demoMode = paay_3D_settings['demo_mode'];    
                
                if (pstep.rebill && rebillAmt != '' && !isNaN(rebillAmt))
                {
                    console.log('rebill:', rebillAmt);
                    postData['rebill'] = rebillAmt;
                    postData['rebillSubmitDelay'] = 0;
                }
                
                if(demoMode)
                {      
                    console.log('sandbox mode');
                    $sandboxURL = "https://sandbox-api.3dsintegrator.com";
                    if(!isLegacy)
                    {
                        $sandboxURL = "https://api-sandbox.3dsintegrator.com/v2"
                    }
                    postData['endpoint'] = $sandboxURL;
                }
                
                postData['prompt'] = promptFn;

                prevString = m+y+cc;
                console.log('New string is passing.');
                count = 0;
                is_process_started = true;
                try{
                    if(count == 0)
                    {
                        $('#cavv').val('');
                        $('#eci').val('');
                        $('#xid').val(''); 
                        $('#rebill_cavv').val('');
                        $('#rebill_eci').val('');
                        $('#rebill_xid').val(''); 
                    }
                    
                    tds = new ThreeDS(
                        formid,
                        paay_3D_settings['api_key'],
                        null,
                        postData,
                        resolve,
                        reject
                    );
                    
                    setTimeout(function(){
                        try
                        {
                            tds.verify(resolve, reject,{amount:amt}).then(function (value1) {
                                console.log('success');
                                return done();
                            }).catch(function (err) {
                                console.log('err');
                                return done();
                            });
                        } catch(err) {
                            return done();
                        }    
                    }, 1500);
                    
                }
                catch(err)
                {
                    $('form').find('[type=submit]').attr('disabled',false);
                    $('#is_verify_done').val('Y');
                    $('#loading-indicator').hide();
                    is_process_started = false;
                    console.log('exception');
                } 

            } else{
                $('form').find('[type=submit]').attr('disabled',false);
                $('#is_verify_done').val('Y');
                $('#loading-indicator').hide();
                is_process_started = false;
                return done();
            }

        });

    }

    function startLoading()
    {
        return new Promise(function (resolve, reject)
        {
            if((typeof paay_3D_settings === 'undefined')){
                return resolve();
            }
            $('#loading-indicator').show();
            var time = timeout;
            var timer = setInterval(function() {
                var is_verify_done = $('#is_verify_done').val();
                var card_status = $('#status').val();
                var isdataFilled = false;
                if(
                    typeof is_verify_done != 'undefined' && 
                    is_verify_done == 'Y' && 
                    typeof card_status != 'undefined' && 
                    (card_status == 'Y' || card_status == 'A' || card_status == 'C' || card_status == 'N' || card_status == 'U')
                )
                {
                    isdataFilled = true;
                }
                time--;
                console.log(time, isdataFilled);
                if(!isTrigger || isdataFilled)
                {
                    console.log(time);
                    clearInterval(timer);
                    return resolve();
                }
                if (time === 0) {
                  clearInterval(timer);
                  return resolve();
                }
            }, 1000);

        });

    }

    function startLoadingNew()
    {
        return new Promise(function (resolve, reject)
        {
            if((typeof paay_3D_settings === 'undefined')){
                return resolve();
            }
            $('#loading-indicator').show();
            //var time = timeout;
            var timer = setInterval(function() {
                var is_verify_done = $('#is_verify_done').val();
                var card_status = $('#status').val();
                var rebill_card_status = $('#rebill_status').val();
                var isdataFilled = false;
                if(
                    typeof is_verify_done != 'undefined' && 
                    is_verify_done == 'Y' && 
                    (typeof card_status != 'undefined' || typeof rebill_card_status != 'undefined') && 
                    (card_status == 'Y' || card_status == 'A' || card_status == 'C' || card_status == 'N' || card_status == 'U' || rebill_card_status == 'Y' || rebill_card_status == 'A' || rebill_card_status == 'C' || rebill_card_status == 'N' || rebill_card_status == 'U')
                )
                {
                    isdataFilled = true;
                }
                //time--;
                //console.log(time, isdataFilled);
                if(!isTrigger || isdataFilled)
                {
                    //console.log(time);
                    clearInterval(timer);
                    return resolve();
                }
                // if (time === 0) {
                //   clearInterval(timer);
                //   return resolve();
                // }
            }, 1000);

        });

    }


    function checkCardDataNew()
    {
        return new Promise(function (done, notdone)
        {
            var errors = [];
            var formName = $('form').attr('name');
            if (paayformArr.indexOf(formName) == -1 || typeof paay_3D_settings === 'undefined')
            {
                return done();
            }          
            
            if (!$('form[name=' + formName + ']').length)
            {
                return done();
            } 
            
            var errors = [];
            var selector = '';
            if (formName == 'is-upsell')
            {
                selector = 'input';
            }
            else
            {
                selector = 'select';
            }

            var validCCType = ['visa', 'master','amex', 'discover'];
            var m = $(selector + '[name=expmonth]').val();
            var y = $(selector + '[name=expyear]').val();
            var cc = $('input[name=creditCardNumber]').val();
            var ccType = $(selector + '[name=creditCardType]').val();
            var currStp = paay_3D_settings.step_id;

            if($.inArray(ccType, validCCType) == '-1' && currStp == 1) {
                console.log('Excluding paay call');
                return done();
            }
            
            var binVerify = paay_3D_settings.enable_bin_verify;
            if(typeof(binVerify) != 'undefined' && binVerify) {
                var unmaskedCardNumber = cc.trim().replace(/[\s-]/g, '');
                if(typeof(unmaskedCardNumber) != 'undefined' && unmaskedCardNumber.length < 6) {
                    return done();
                }
                var binData = cb.decryptAllowedTestCard(paay_3D_settings.bin);
                var binArray = binData.split('\n');
                var checkingType = paay_3D_settings.action_on_bin;
                var bin = unmaskedCardNumber.substr(0,6);
                var isBinInList = $.inArray(bin, binArray);
                if(typeof(checkingType) != 'undefined' && checkingType == 'exclude' && isBinInList != '-1') {
                    if(typeof(dev_mode) != 'undefined' && dev_mode == 'Y')
                    {
                        console.log('Excluded');
                    }
                    return done();
                }
                
                if(typeof(checkingType) != 'undefined' && checkingType == 'include' && isBinInList == '-1') {
                    if(typeof(dev_mode) != 'undefined' && dev_mode == 'Y')
                    {
                        console.log('Not Included');
                    }
                    return done();
                }               
            }

            cc = cc.replace(/-/g, '');
            var t = $('[name=x_transaction_id]').val();//randomString(6);
            var amt = parseFloat($('#amt').val());
            var rebillAmt = parseFloat($('#rebill_amt').val());
            var pstep = paay_3D_settings['step' + currStp];  
            
            /*
             * Start New Card Type Filter Logic
             */
            var cardTypeSettings = pstep['card_type']; 
            if(typeof(cardTypeSettings) != 'undefined' && cardTypeSettings.length) {
                paay_3D_settings.only_rebill = false;
                pstep.rebill = true;
                pstep.main = true;
                pstep.split_main = true;
                pstep.split_rebill = true;
                
                if(
                    cardTypeSettings.indexOf('rebill_'+ccType) == -1 &&  
                    cardTypeSettings.indexOf('rebill_initial_'+ccType) == -1 &&
                    cardTypeSettings.indexOf('initial_rebill_'+ccType) == -1)
                {
                    pstep.rebill = false;
                }
                
                if(
                    cardTypeSettings.indexOf('initial_'+ccType) == -1 &&  
                    cardTypeSettings.indexOf('rebill_initial_'+ccType) == -1 &&
                    cardTypeSettings.indexOf('initial_rebill_'+ccType) == -1)
                {                    
                    pstep.main = false;
                    paay_3D_settings.only_rebill = true;
                }

                if(
                    cardTypeSettings.indexOf('split_initial_'+ccType) == -1)
                {
                    pstep.split_main = false;
                }

                if(
                    cardTypeSettings.indexOf('split_rebill_'+ccType) == -1)
                {
                    pstep.split_rebill = false;
                }

            } 
            /*
             * End New Card Type Filter Logic
             */
            
            if (!pstep.main && paay_3D_settings.only_rebill)
            {
                amt = rebillAmt;
                rebillAmt = '';
            }
            
            var errorflag = false;
            var tdsmain;
            var tdsrebill;
            var mainflow = false;
            var rebillflow = false;
            var splitmainflow = false;
            var splitrebillflow = false;

            if (m != '' && y != '' && cc != '' && t != '' && amt != '' && (pstep.main || pstep.rebill || pstep.split_main || pstep.split_rebill))
            {
                try
                {
                    if(is_process_started)
                    {
                        console.log('ANOTHER PROCESS IS ALREADY RUNNING, PLEASE WAIT...');
                        return done();
                    }
                    if(prevString!= '' && prevString == (m+y+cc))
                    {
                        console.log('NOTHING HAS CHANGED');
                        return done();
                    }

                    isTrigger = true;
                    mainflow = true;
                    totalInitialisation = 1;
                    $('#is_verify_done').val('N');
                    console.log('after initial:', amt, cc, m, y, t);
                    var strict_method = paay_3D_settings['strict_method'];
                    if(strict_method)
                    {
                        $('form').find('[type=submit]').attr('disabled','disabled');
                    }
                    var formid = $('form[name=' + formName + ']').attr('id');
                    timeout = paay_3D_settings['max_timeout'];
                    var postData = {
                        autoSubmit:false, 
                        allowRetry: false, 
                        iframeId: "paay-iframe", 
                        forcedTimeout: timeout
                    };
                    if(typeof(dev_mode) != 'undefined' && dev_mode == 'Y')
                    {
                        postData['verbose'] = true;
                    }

                    if (typeof formid === 'undefined')
                    {
                        $('form[name=' + formName + ']').attr('id', formName);
                        formid = formName;
                    }
                    var demoMode = paay_3D_settings['demo_mode'];    

                    if (pstep.rebill && rebillAmt != '' && !isNaN(rebillAmt))
                    {
                        console.log('rebill:', rebillAmt);
                        rebillflow = true;
                        totalInitialisation = 2;
                    }

                    if (pstep.split_main)
                    {
                        var splitMainAmt = parseFloat($('#split_amt').val());
                        console.log('split main:', splitMainAmt);
                        if(splitMainAmt && !isNaN(splitMainAmt)){
                            splitmainflow = true;
                            totalInitialisation = 3;
                        }
                    }

                    if (pstep.split_rebill)
                    {
                        var splitRebillAmt = parseFloat($('#split_rebill_amt').val());
                        console.log('split rebill:', splitRebillAmt);
                        if(splitRebillAmt && !isNaN(splitRebillAmt)){
                            splitrebillflow = true;
                            totalInitialisation = 4;
                        }
                    }


                    if(demoMode)
                    {      
                        console.log('sandbox mode');
                        $sandboxURL = "https://sandbox-api.3dsintegrator.com";
                        if(!isLegacy)
                        {
                            $sandboxURL = "https://api-sandbox.3dsintegrator.com/v2"
                        }
                        postData['endpoint'] = $sandboxURL;
                    }

                    postData['prompt'] = promptFn;
                    postData['addResultToForm'] = false;

                    prevString = m+y+cc;
                    console.log('New string is passing.');
                    count = 0;
                    is_process_started = true;
                    try
                    {
                        // Main Inialization
                        tdsmain = new ThreeDS(
                            formid,
                            paay_3D_settings['api_key'],
                            null,
                            postData
                        );

                        // Rebill Inialization
                        tdsrebill = new ThreeDS(
                            formid,
                            paay_3D_settings['api_key'],
                            null,
                            postData
                        );
    
                        if(rebillflow){
                            // Main Call
                            tdsmain.verify(authentication_completed_initial, authentication_failed_initial,{amount:amt});
                            
                            // Rebill Call
                            tdsrebill.verify(authentication_completed_rebill, authentication_failed_rebill,{amount:rebillAmt});
                        } else if(!pstep.main && paay_3D_settings.only_rebill) {
                            //Only Rebill for main
                            totalInitialisation = 1;
                            tdsrebill.verify(authentication_completed_rebill, authentication_failed_rebill,{amount:amt});
                        } else {
                            // Main Call where no rebill
                            tdsmain.verify(authentication_completed_initial, authentication_failed_initial,{amount:amt});
                        }


                        if(splitmainflow){
                            //  Split Main Inialization
                            var tdssplitmain = new ThreeDS(
                                formid,
                                paay_3D_settings['api_key'],
                                null,
                                postData
                            );
                            tdssplitmain.verify(authentication_completed_initial_split, authentication_failed_initial_split,{amount:splitMainAmt});
                        }

                        if(splitrebillflow){
                            // Split Rebill Inialization
                            var tdssplitrebill = new ThreeDS(
                                formid,
                                paay_3D_settings['api_key'],
                                null,
                                postData
                            );
                            tdssplitrebill.verify(authentication_completed_rebill_split, authentication_failed_rebill_split,{amount:splitRebillAmt});
                        }

                        return done();
                        
                    } catch(err) {
                        console.log('Err:', err);
                        return done();
                    }    
                    
                }
                catch(err)
                {
                    $('form').find('[type=submit]').attr('disabled',false);
                    $('#is_verify_done').val('Y');
                    $('#loading-indicator').hide();
                    is_process_started = false;
                    console.log('exception', err);
                    return done();
                } 

            } else{
                $('form').find('[type=submit]').attr('disabled',false);
                $('#is_verify_done').val('Y');
                $('#loading-indicator').hide();
                is_process_started = false;
                return done();
            }

        });

    }

    if(enable_loader)
    {
        if(typeof event_type !== 'undefined' && event_type == 'blur') {
            cb.beforeFormSubmitEvents.push(function (formElement, formOptions) {
                startLoading().then(function (value1) {
                    deleteSavedCookies();
                    cb.submitForm(formElement, formOptions);
                });

            });
        } else if(typeof event_type !== 'undefined' && event_type == 'submit') {       
            cb.beforeFormSubmitEvents.push(function (formElement, formOptions) {
                checkCardData().then(function (value1) {
                    startLoading().then(function (value2) {
                        deleteSavedCookies();
                        cb.submitForm(formElement, formOptions);
                    });
                });
            }); 
              
        } else {
           cb.beforeFormSubmitEvents.push(function (formElement, formOptions) {
                checkCardDataNew().then(function (value1) {
                    startLoadingNew().then(function (value2) {
                        deleteSavedCookies();
                        console.log('on submit');
                        cb.submitForm(formElement, formOptions);
                    });
                });
            }); 
        }       
    }
    
    function promptFn()
    {  
       console.log('prompt response');
       $('#is_verify_done').val('Y');      
    }

    //initial callback functions
    var authentication_completed_initial = function (response) {
        console.log("initial success!")
        console.log(response);

        initial_xid = response.dsTransId
        initial_cavv = response.authenticationValue
        initial_eci = response.eci
        initial_authenticationValue = response.authenticationValue
        initial_dsTransactionId = response.dsTransId
        initial_status = response.status ? response.status : 'N'
        initial_protocolVersion = response.protocolVersion
        initial_and_rebill_complete()
    }
    var authentication_failed_initial = function (response) {
        console.log("initial failure!")
        console.log(response)
        initial_and_rebill_complete()
    }


    //rebill callback functions
    var authentication_completed_rebill = function (response) {
        console.log("rebill success!")
        console.log(response)
        rebill_xid = response.dsTransId
        rebill_cavv = response.authenticationValue
        rebill_eci = response.eci
        rebill_authenticationValue = response.authenticationValue
        rebill_dsTransactionId = response.dsTransId
        rebill_protocolVersion = response.protocolVersion
        rebill_status = response.status ? response.status : 'N'
        initial_and_rebill_complete()
    }
    var authentication_failed_rebill = function (response) {
        console.log("rebill failure!")
        console.log(response)
        initial_and_rebill_complete()
    }

    function initial_and_rebill_complete() {
        try{
            count++
            //this function only displays once both callbacks have returned
            if (count == totalInitialisation) {
                //once this function is called twice
                //once for initial and once for rebill
                $('#is_verify_done').val('Y');      
                is_process_started = false;
                console.log("hit "+totalInitialisation+" times");

                // Mapping Variables

                $('[name=cavv]').val(initial_cavv);
                $('[name=eci]').val(initial_eci);
                $('[name=xid]').val(initial_xid);
                $('[name=authenticationValue]').val(initial_authenticationValue);
                $('[name=dsTransactionId]').val(initial_dsTransactionId);
                initial_status = initial_status ? initial_status : 'N';
                $('[name=status]').val(initial_status);
                $('[name=protocolVersion]').val(initial_protocolVersion);

                $('[name=rebill_cavv]').val(rebill_cavv);
                $('[name=rebill_eci]').val(rebill_eci);
                $('[name=rebill_xid]').val(rebill_xid);
                $('[name=rebill_authenticationValue]').val(rebill_authenticationValue);
                $('[name=rebill_dsTransactionId]').val(rebill_dsTransactionId);
                rebill_status = rebill_status ? rebill_status : 'N'
                $('[name=rebill_status]').val(rebill_status);
                $('[name=rebill_protocolVersion]').val(rebill_protocolVersion);

                $('[name=split_cavv]').val(split_cavv);
                $('[name=split_eci]').val(split_eci);
                $('[name=split_xid]').val(split_xid);
                $('[name=split_authenticationValue]').val(split_authenticationValue);
                $('[name=split_dsTransactionId]').val(split_dsTransactionId);
                split_status = split_status ? split_status : 'N';
                $('[name=split_status]').val(split_status);
                $('[name=split_protocolVersion]').val(split_protocolVersion);

                $('[name=split_rebill_cavv]').val(split_rebill_cavv);
                $('[name=split_rebill_eci]').val(split_rebill_eci);
                $('[name=split_rebill_xid]').val(split_rebill_xid);
                $('[name=split_rebill_authenticationValue]').val(split_rebill_authenticationValue);
                $('[name=split_rebill_dsTransactionId]').val(split_rebill_dsTransactionId);
                split_rebill_status = split_rebill_status ? split_rebill_status : 'N'
                $('[name=split_rebill_status]').val(split_rebill_status);
                $('[name=split_rebill_protocolVersion]').val(split_rebill_protocolVersion);

                // var myJSON = JSON.stringify(konnektive);
                // document.getElementById("display").innerHTML = `<br> <p>` +myJSON +`</p>   <br> <p> Both callbacks have returned, you can now submit order to Konneketive</p>`;
            }
        }
        catch(err){
            $('#is_verify_done').val('Y'); 
            $('[name=status]').val('N');
            $('[name=rebill_status]').val('N');
            $('[name=split_status]').val('N');
            $('[name=split_rebill_status]').val('N');
            is_process_started = false;
        }
        

    }

    // Split Initial
    var authentication_completed_initial_split = function (response) {
        console.log("split initial success!")
        console.log(response)
        split_xid = response.dsTransId
        split_cavv = response.authenticationValue
        split_eci = response.eci
        split_authenticationValue = response.authenticationValue
        split_dsTransactionId = response.dsTransId
        split_status = response.status ? response.status : 'N'
        split_protocolVersion = response.protocolVersion
        initial_and_rebill_complete()
    }
    var authentication_failed_initial_split = function (response) {
        console.log("split initial failure!")
        console.log(response)
        initial_and_rebill_complete()
    }

    // Split Rebill
    var authentication_completed_rebill_split = function (response) {
        console.log("split rebill success!")
        console.log(response)
        split_rebill_xid = response.dsTransId
        split_rebill_cavv = response.authenticationValue
        split_rebill_eci = response.eci
        split_rebill_authenticationValue = response.authenticationValue
        split_rebill_dsTransactionId = response.dsTransId
        split_rebill_status = response.status ? response.status : 'N'
        split_rebill_protocolVersion = response.protocolVersion
        initial_and_rebill_complete()
    }
    var authentication_failed_rebill_split = function (response) {
        console.log("split rebill failure!")
        console.log(response)
        initial_and_rebill_complete()
    }

});

    function storeCookie(data, type){
        var currStp = paay_3D_settings.step_id;
        if(currStp > 1)
        {
            if(type == 'main')
            {
                document.cookie = 'cavv ='+data.cavv+';';
                document.cookie = 'eci ='+data.eci+';';
                document.cookie = 'xid ='+data.xid+';';
            }
            else{
                document.cookie = 'rebill_cavv ='+data.cavv+';';
                document.cookie = 'rebill_eci ='+data.eci+';';
                document.cookie = 'rebill_xid ='+data.xid+';';
            }
            
        }
    }

    function deleteSavedCookies(){
        
        var paay_params = ["cavv", "eci", "xid", "rebill_cavv", "rebill_eci", "rebill_xid"];
        paay_params.forEach(function(index){
            document.cookie = index+'=; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        })
    }

    function placeCookies(){
        var maindata = getSavedCookie('main');
        var rebilldata = getSavedCookie('rebill');
        var isPrevDataFound = false;

        if(maindata.cavv && maindata.eci && maindata.xid)
        {
            $('[name=cavv]').val(maindata.cavv);
            $('[name=eci]').val(maindata.eci);
            $('[name=xid]').val(maindata.xid);
            isPrevDataFound = true;
            console.log('Already evaluated for Initial.')
        }

        if(rebilldata.rebill_cavv && rebilldata.rebill_eci && rebilldata.rebill_xid)
        {
            $('[name=rebill_cavv]').val(rebilldata.rebill_cavv);
            $('[name=rebill_eci]').val(rebilldata.rebill_eci);
            $('[name=rebill_xid]').val(rebilldata.rebill_xid);  
            isPrevDataFound = true;
            console.log('Already evaluated for Rebill.')
        }
        if(isPrevDataFound)
        {
            console.log('Already evaluated from storage.');
        }

        return isPrevDataFound;
    }


    function getSavedCookie(type) {
        var currStp = paay_3D_settings.step_id;
        var initial_paay = ["cavv", "eci", "xid"];
        var rebill_paay = ["rebill_cavv", "rebill_eci", "rebill_xid"];
        var result_array = [];
        if(currStp > 1)
        {
            var allcookies = document.cookie;
            cookiearray = allcookies.split(';');

            for(var i=0; i<cookiearray.length; i++) {
               name = cookiearray[i].split('=')[0];
               value = cookiearray[i].split('=')[1];
                name = name.trim();
               if(type == 'main' && initial_paay.indexOf(name) != -1)
               {
                   result_array[name] = value;
               }
               else if(type == 'rebill' && rebill_paay.indexOf(name) != -1)
               {
                   result_array[name] = value;
               }
            }
         }
        return result_array;
     }

    function resolve(data)
    {  
        $('form').find('[type=submit]').attr('disabled',false);
        var rebill_method_first = paay_3D_settings.rebill_method_first;

        var currStp = paay_3D_settings.step_id;
        var pstep = paay_3D_settings['step' + currStp];  
        var isRebillEnabled = pstep.rebill;
        
        if(!isRebillEnabled)
        {
            $('#is_verify_done').val('Y');
            is_process_started = false;
        }

        if(count == 0)
        {
            if(rebill_method_first || paay_3D_settings.only_rebill)
            {
                $('#rebill_cavv').val(data.cavv);
                $('#rebill_eci').val(data.eci);
                $('#rebill_xid').val(data.xid);  
                Cavv = $('#rebill_cavv').val();
                Eci = $('#rebill_eci').val();
                Xid = $('#rebill_xid').val();
                $('#cavv').val('');
                $('#eci').val('');
                $('#xid').val('');
                $('[name=cavv]').val('');
                $('[name=eci]').val('');
                $('[name=xid]').val('');
                $('#is_verify_done').val('Y');
                if(paay_3D_settings.only_rebill)
                {
                    is_process_started = false;
                }
            }
            storeCookie(data, 'main');
            console.log('main auth data::',data);
            count = count+1;
            return;
        }

        if(isRebillEnabled)
        {
            $('#is_verify_done').val('Y');
            is_process_started = false;
        }

        storeCookie(data, 'rebill');
        console.log('rebill auth data::',data);
        
        if(rebill_method_first)
        {
            $('#cavv').val(data.cavv);
            $('#eci').val(data.eci);
            $('#xid').val(data.xid); 
            $('#rebill_cavv').val(Cavv);
            $('#rebill_eci').val(Eci);
            $('#rebill_xid').val(Xid);  
        }
        
        $('#loading-indicator').hide();        
    }
    
    function reject(data)
    {
        console.log('error',data);
        $('form').find('[type=submit]').attr('disabled',false);
        $('#loading-indicator').hide();
        $('#is_verify_done').val('Y');
        if(!$('#paay-iframe').length){
            $('#status').val('N');  
        }   
        is_process_started = false;
    }


